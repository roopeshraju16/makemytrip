# MakeMyTrip

Have used Page Object Model for to develop the test automation suite 

Tools: 
1. Programing language: [Java]
2. UI Automation Framework Lib: [Selenium]
3. Build tool: [Maven] 
4. BDD framework for to write tests: [Cucumber]
5. practice.Test Reports Generation: []
6. Make sure [Cucumber for Java] is installed in the editor


Implementation: 
1. The test scenarios/ business use case written in .feature file
2. The test scenarios mapped to java method's under step classes in step folder
3. The page level actions are under Page classes in page folder
4. The page level locators are under Locator classes in locator folder
5. The WebDriver Initialisation & multiple usage of methods implementation done in Base Class 
6. Url, User Details will be fetched from properties file
7. practice.Test data will be fetched from .csv file  


