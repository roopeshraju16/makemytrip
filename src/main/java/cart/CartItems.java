package cart;

public class CartItems {

   private String menu_item_id;
   private long quantity;

   public void setMenu_item_id(String menu_item_id){
       this.menu_item_id=menu_item_id;
   }

   public String getMenu_item_id(){
       return menu_item_id;
   }

   public void setQuantity(long quantity){
       this.quantity=quantity;
   }

   public long getQuantity(){
       return quantity;
   }
}
