package cart;

import java.util.List;

public class UpdateCart {


    private List<CartItems> cartItems;

    private long restaurantId;

    private String cart_type;

    private String address_id;

    private String couponCode;

    public void setRestaurantId(long restaurantId){
        this.restaurantId=restaurantId;
    }

    public long getRestaurantId() {
        return restaurantId;
    }

    public void setCart_type(String cart_type){
        this.cart_type=cart_type;
    }

    public String getCart_type(){
        return cart_type;
    }

    public void setAddress_id(String address_id){
        this.address_id=address_id;
    }

    public String getAddress_id(){
        return address_id;
    }

    public void setCouponCode(String couponCode){
        this.couponCode=couponCode;
    }

    public String getCouponCode(){
        return couponCode;
    }

    public void setCartItems(List<CartItems> cartItems){
        this.cartItems=cartItems;
    }

    public List<CartItems> getCartItems(){
        return cartItems;
    }

}
