package cart;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.ArrayList;
import java.util.List;

public class Swiggy
{

//    curl -X POST \
//    http://kong.d1.swiggyops.de/api/v2/cart/client/update \
//        -H 'Authorization: test' \
//        -H 'Content-Type: application/json' \
//        -H 'Postman-Token: c36fd75d-853a-41e7-b9d7-8c780511c9dc' \
//        -H 'Tid: test' \
//        -H 'User-Agent: cart.Swiggy-Android' \
//        -H 'token: test' \
//        -H 'version-code: 450' \
//        -d '{
//        "cartItems": [
//    {
//
//        "menu_item_id": "12345",
//            "quantity": 1
//    }
//    ],
//            "restaurantId": 123,
//            "cart_type": "REGULAR",
//
//            "address_id": "12356",
//
//            "couponCode" : "JUMBO"
//}'

    UpdateCart UC = new UpdateCart();
    CartItems CI = new CartItems();

   public Response setUpRequest(String menu_item_id
           ,long quantity,long restaurantId,String cart_type,String address_id,String couponCode){
       CI.setMenu_item_id(menu_item_id);
       CI.setQuantity(quantity);
       List<CartItems> ls = new ArrayList<>();
       ls.add(CI);
      UC.setCartItems(ls);
      UC.setRestaurantId(restaurantId);
      UC.setCart_type(cart_type);
      UC.setAddress_id(address_id);
      UC.setCouponCode(couponCode);

      Response cartResponse = RestAssured.given().headers("Authorization","test").headers("Content-Type","application/json")
               .headers("Tid","test").headers("User-Agent","cart.Swiggy-Android").headers("token","test")
               .headers("Accept","application/json")
               .headers("version-code",450).log().all().when().body(UC).post("http://kong.d1.swiggyops.de/api/v2/cart/client/update").then().assertThat().statusCode(200)
               .extract().response();
      return cartResponse;
   }


   public void extractResponse(Response response){
       System.out.println(response.prettyPrint());
   }

}
