package practice;

import java.util.Arrays;
import java.util.TreeSet;

public class MergingTwoArrays {

    public static void mergeTwoArrays(){
        int a[] = new int[]{1, 3, 5, 7};
        int b[]= new int[]{0, 2, 6, 8, 9};

        TreeSet<Integer> ts = new TreeSet<>();
        for(int i=0; i<=a.length-1; i++){
            for(int j=0; j<=b.length-1; j++){
                ts.add(a[i]);
                ts.add(b[j]);
            }
        }
        System.out.println(ts);
        System.out.println(ts.descendingSet());
    }


    public static void main(String[] args) {
        mergeTwoArrays();
    }
}
