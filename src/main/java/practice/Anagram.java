package practice;

import java.util.*;

public class Anagram {

    public static boolean anagramLogic(){
        String s1 = "ACT";
        String s2 = "CAT";
        char[] sToChar = s1.toCharArray();
        StringBuilder builder = new StringBuilder(s2);
        boolean flag = false;
        for(char c: sToChar){
            int index = builder.indexOf(""+c);
            if(index !=-1){
                    builder.deleteCharAt(index);
                    flag = true;
            } else {
                flag = false;
                break;
            }
        }
        return flag;
    }

    public static boolean secondFindAnagram() {
        boolean flag = false;
        String[] s1 = {"A", "C", "T"};
        String[] s2 = {"T", "A", "A"};

        Arrays.sort(s1);
        Arrays.sort(s2);

        if (s1.length != s2.length)
            return flag;

        for (int i = 0; i < s1.length; i++)
            if (s1[i] != s2[i])
                return flag;
        return true;
    }

    public static List<List<String>> fetchGroupOfAnagrams(String[] arrayValue){
        List<List<String>> s = new ArrayList<>();
        HashMap<String,ArrayList<String>> map = new HashMap<>();
        for(String strs: arrayValue){
            char[] chars = strs.toCharArray();
            Arrays.sort(chars);
            String sorted = new String(chars);

            ArrayList<String> list = map.getOrDefault(sorted,new ArrayList<>());

            list.add(strs);

            map.put(sorted,list);
        }

        for(Map.Entry<String,ArrayList<String>> k: map.entrySet()){
            ArrayList<String> L = k.getValue();
            s.add(L);
        }
        return s;
    }

    public static String[] sendGroupOfArrays(){
        String[] s = {"eat","tea","tan","ate","nat","bat"};
        return s;
    }

    public static void main(String[] args) {
        System.out.println(secondFindAnagram());
//        System.out.println(fetchGroupOfAnagrams(sendGroupOfArrays()));
    }

}
