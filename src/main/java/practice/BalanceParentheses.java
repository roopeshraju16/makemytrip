package practice;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Stack;

public class BalanceParentheses {

    public static boolean checkParentheses(String parentheses){
        boolean flag = false;

        Stack<Character> s = new Stack<>();

        for(int i=0; i<parentheses.length()-1; i++){
            char sChar =parentheses.charAt(i);
            if(sChar=='('||sChar=='['||sChar=='{'){
                s.push(sChar);
                continue;
            }
            char cChar = parentheses.charAt(i);
            switch (cChar){
                case ')':
                 cChar = s.pop();
                    if (cChar == '{' || cChar == '[')
                        flag = false;
                 break;
                case ']':
                  cChar = s.pop();
                    if (cChar == '(' || cChar == '{')
                        flag = false;
                    break;
                case '}':
                  cChar = s.pop();
                    if (cChar == '[' || cChar == '(')
                        flag = false;
                    break;
            }

        }
        return flag;
    }

    static boolean areBracketsBalanced(String expr)
    {
        // Using ArrayDeque is faster than using Stack class
        Deque<Character> stack
                = new ArrayDeque<>();

        // Traversing the Expression
        for (int i = 0; i < expr.length(); i++)
        {
            char x = expr.charAt(i);

            if (x == '(' || x == '[' || x == '{')
            {
                // Push the element in the stack
                stack.push(x);
                continue;
            }

            // If current character is not opening
            // bracket, then it must be closing. So stack
            // cannot be empty at this point.
            if (stack.isEmpty())
                return false;
            char check;
            switch (x) {
                case ')':
                    check = stack.pop();
                    if (check == '{' || check == '[')
                        return false;
                    break;

                case '}':
                    check = stack.pop();
                    if (check == '(' || check == '[')
                        return false;
                    break;

                case ']':
                    check = stack.pop();
                    if (check == '(' || check == '{')
                        return false;
                    break;
            }
        }

        // Check Empty Stack
        return (stack.isEmpty());
    }


    public static void main(String[] args) {
        String s = "([{}])";

        if(areBracketsBalanced(s))
        {
            System.out.println("Balanced");
        }else {
            System.out.println("Not Balanced");
        }
    }
}
