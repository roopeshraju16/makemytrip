package practice;

import java.util.Scanner;

public class TransposeOfMatrix {
    public static void main(String[] args) {

        System.out.println("Enter rows and columns");
        Scanner sc = new Scanner(System.in);
        int rows = sc.nextInt();
        int columns = sc.nextInt();
        int[][] a = new int[rows][columns];
        int [][] b = new int[columns][rows];

        System.out.println("Enter the matrix values");

        for(int i=0; i<rows; i++){
            for(int j=0; j<columns; j++){
                a[i][j] = sc.nextInt();
            }
        }

        System.out.println("Transpose of a matix");
        for(int k=0; k<rows;k++){
            for(int l=0; l<columns; l++){
               b[l][k]=a[k][l];
                System.out.println(b[l][k]);
            }
            System.out.println();
        }

    }

}
