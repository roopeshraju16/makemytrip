package practice;

public class PermutationString {

    public static void permute(String str,String ans){
        // If string is empty
        if (str.length() == 0) {

            // print ans
            System.out.print(ans + " ");
            return;
        }


        for(int i=0; i<=str.length()-1;i++){
            char c = str.charAt(i);
            String value = str.substring(0,i)+str.substring(i+1);
            //Recursive call
            permute(value,ans+c);
        }

    }

    public static void main(String[] args) {
          permute("ABC","");
    }
}
