package practice;

import java.util.*;
import java.util.stream.Stream;

public class LLProblem {


    public static void reverseLinkedList(LinkedList<Integer> integerLinkedList){
        System.out.println(integerLinkedList);

        for(int i=integerLinkedList.size()-1; i>=0; i--){
            System.out.println(integerLinkedList.get(i));
        }

    }

    public static LinkedList<Integer> reverseList(){
        LinkedList<Integer> lI = new LinkedList<>();
        lI.add(1);
        lI.add(2);
        lI.add(3);
        lI.add(4);
        lI.add(5);
        lI.add(6);
        return lI;
    }

    public static void sortLinkedList(LinkedList<Integer> integerLinkedList){
        Stream<Integer> value= integerLinkedList.stream().sorted();
       Iterator check= value.iterator();
        while(check.hasNext()){
            System.out.println(check.next());
        }
    }



    public static LinkedList<Integer> getStaticLinkedList(){
        LinkedList<Integer> lI = new LinkedList<>();
        lI.add(1);
        lI.add(2);
        lI.add(2);
        lI.add(1);
        lI.add(2);
        lI.add(0);
        lI.add(2);
        lI.add(2);
        return lI;
    }

    public static LinkedList<Integer> getLinkedList(){
        LinkedList<Integer> lI = new LinkedList<>();
        Scanner sc  = new Scanner(System.in);
        int n = sc.nextInt();
        for(int i=0; i<=n;i++){
            lI.add(sc.nextInt());
        }
        return lI;
    }

    public static void main(String[] args) {
//        sortLinkedList(getStaticLinkedList());
        reverseLinkedList(reverseList());
    }
}
