package practice;

public class PermutationNumber {

    public static void main(String[] args) {
        int totalNumberOfObjects = 6;
        int totalNumberOfRepetations = 2;
        int f1 = totalNumberOfObjects,f2;
        for(int i=totalNumberOfObjects-1; i>=1; i--){
            f1 = f1 * i;
        }
        f2 = totalNumberOfObjects-totalNumberOfRepetations;
        for(int j=f2-1;j>=1; j--){
            f2= f2 * j;
        }
        int permutation = f1/f2;
        System.out.println(permutation);
    }

}
