package practice;

import java.util.HashMap;
import java.util.Map;

public class Test {


    public static void main(String[] args) {
        String actualString="Im an automation engineer";

        HashMap<Character,Integer> hM = new HashMap<>();

        String[] conversionToStringArray = actualString.split(" ");

        String res ="";

        for(int i=0;i<=conversionToStringArray.length-1; i++){

            String s = conversionToStringArray[i];

            char[] c = s.toCharArray();
            for(int k=0;k<=c.length-1; k++){
                if(hM.containsKey(c[k])){
                    hM.put(c[k],hM.get(c[k])+1);
                }else{
                    hM.put(c[k],1);
                }
            }

        }

        for(Map.Entry<Character, Integer> m: hM.entrySet()){
            System.out.println(m.getKey()+" "+m.getValue());
             res+= m.getKey();
        }
    }

}
