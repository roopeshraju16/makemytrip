package practice;

public class ReverseInteger {

    public static void main(String[] args) {
        int number = 1234567;
        String numberToInt = String.valueOf(number);
        String newStringFormatNumber ="";

        for(int i=numberToInt.length()-1; i>=0; i--){
            newStringFormatNumber = newStringFormatNumber+numberToInt.charAt(i);
        }
        System.out.println(Integer.valueOf(newStringFormatNumber.trim()));
    }
}
