package practice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ConversionOfListToSet {

    public static void main(String[] args) {
        Set<Integer> s =new HashSet<>();
        s.add(1);
        s.add(2);
        s.add(9);
        s.add(1);
        s.add(2);
        s.add(3);
        s.add(1);
        s.add(4);
        s.add(1);
        s.add(5);
        s.add(7);
        System.out.println(s);

        int n = s.size();
        List<Integer> aList = new ArrayList<>(n);
        for (Integer x : s)
            aList.add(x);

        System.out.println("List");
        for(Integer x: aList){
            System.out.println(x);
        }
    }
}
