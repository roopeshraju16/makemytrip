package practice;

import java.util.*;

public class PhonePe {

public static boolean getBoolean(String str1, String str2){
     boolean flag = false;
    Stack<String> sc = new Stack<>();
    String expectedString ="";
     if(str1.length()==0||str2.length()==0){
         return flag;
     }
     else {
         char[] cH1 = str1.toCharArray();//A,X,Y
         char[] cH2 = str2.toCharArray();//A,D,X,C,P,Y
         for(int i=0;i<=cH1.length-1;i++){
             for(int j=0;j<=cH2.length-1; j++){
                  if(cH1[i]==cH2[j]){
                     sc.push(""+cH2[j]);
                  }
             }
         }

         for(int k=0; k<=sc.size()-1;k++){
             expectedString+=sc.get(k).trim();
         }

         if(expectedString.equalsIgnoreCase(str1)){
             flag = true;
             return flag;
         }else {
             return flag;
         }
     }
}


public static List<List<String>> fetchAnagrams(String[] input){

    HashMap<String,ArrayList<String>> map = new HashMap<>();
    List<List<String>> s = new ArrayList<>();

    for(int i=0;i<=input.length-1;i++){
        char[] strs = input[i].toCharArray();
        Arrays.sort(strs);
        String sorted = new String(strs);
        ArrayList<String> sL =  map.getOrDefault(sorted,new ArrayList<>());
        sL.add(input[i]);
        map.put(sorted,sL);
    }

    for(Map.Entry<String,ArrayList<String>> m:map.entrySet())
    {
        ArrayList<String> Value = m.getValue();
        s.add(Value);

    }
    return s;
}


    public static void main(String[] args) {
//        System.out.println(getBoolean("AXY","ADYCPX"));
        String[] str = {"god","cat","dog","fan","tac","dgo"};
        System.out.println(fetchAnagrams(str));
    }
}
