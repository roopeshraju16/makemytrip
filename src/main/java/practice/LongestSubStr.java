package practice;

import java.util.HashMap;
import java.util.LinkedHashSet;

public class LongestSubStr {

    public static void thirdLogic(){
        LinkedHashSet<Character> set = new LinkedHashSet<>();
        String[] longestString = {"aabdcsa","koplmk","olm"};
        for(int i=0; i<=longestString.length-1; i++){
            char[] c = longestString[i].toCharArray();
            for (int j=0; j<=c.length-1; j++){
                if(!set.contains(c[j])){
                    set.add(c[j]);
                }
            }
            System.out.println(set);
        }

    }


    public static void secondLogic(){
        LinkedHashSet<Character> set = new LinkedHashSet<>();
        String longestString = "aabdcsa";
        char[] stringToCharArray = longestString.toCharArray();
        for(int i=0; i<=stringToCharArray.length-1; i++){
            if(!set.contains(stringToCharArray[i])){
                set.add(stringToCharArray[i]);
            }
        }
        System.out.println(set);
    }

    public static void main(String[] args) {
        String longestString = "aabdcsa";
        HashMap<Character,Integer> map = new HashMap<>();
        char[] charArray = longestString.toCharArray();
        for(int i=0; i<charArray.length-1; i++){
            char ch = charArray[i];
            if(!map.containsKey(ch)){
                map.put(ch,i);
            }else {
                i = map.get(ch);
                map.clear();
            }
        }
        System.out.println(map.entrySet().toString());
//        thirdLogic();
//        secondLogic();
    }


}
