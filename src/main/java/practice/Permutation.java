package practice;
import java.util.HashSet;

public class Permutation {
    static String actualValue = "BAT";

    public void findPermutation(char[] c,int i, int n){
        HashSet<String> set = new HashSet<>();
         if(i==n){
             if(!(set.contains(c))){
                 set.add(String.copyValueOf(c));
                 System.out.println(set);
             } else {
                 for (int j = i; j <= n; j++) {
                     char temp = c[i];
                     c[i] = c[j];
                     c[j] = temp;


                     findPermutation(c, i + 1, n);

                     temp = c[i];
                     c[i] = c[j];
                     c[j] = temp;
                 }
             }
         }
        }


    public static void main(String[] args) {
        Permutation permutation = new Permutation();
        char ch[] = actualValue.toCharArray();
        permutation.findPermutation(ch,0,ch.length-1);
    }
}
