package practice;

import java.util.HashMap;
import java.util.Map;

public class Black {


       //Sentence geeks for geeks is the something
      //print

    static String sentence = "geeks for geeks is the something";

    public static String fetchOccurance(String sentence){
        String repetativeWord = "";
       String[] strArray =  sentence.split(" ");
        HashMap<String,Integer> map = new HashMap<>();

       for(int i=0;i<=strArray.length-1;i++){
           if(map.containsKey(strArray[i])){
               map.put(strArray[i],map.get(strArray[i])+1);
           }
           else{
               map.put(strArray[i],1);
           }
       }

       for(Map.Entry<String,Integer> m:map.entrySet()){
            if(m.getValue()>=2){
                repetativeWord = m.getKey();
            }
       }

       if(repetativeWord==""){
           System.out.println("There seems to be no repetative word");
       }

       return repetativeWord;
    }

    public static void main(String[] args) {
        System.out.println(fetchOccurance("hey buddy wassup"));
    }

}
