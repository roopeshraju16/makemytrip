package practice;

public class Swap {

    public static void swapString(String a,String b){
        a= a+b;
        b= a.substring(0,a.length() - b.length());
        a= a.substring(b.length());
        System.out.println("Print of Two Variables: "+a +" "+ b);
    }

    public static void swapInt(int a,int b){
        a=a+b;
        b=a-b;
        a=a-b;
        System.out.println("Print the Swapping of Two Variables: "+a +" "+ b);
    }

    public static void main(String[] args) {
        swapString("Hello","world");
        swapInt(1,2);
    }

}
