package practice;

public class Reverse {

    public static void main(String[] args) {
        String actual = "selenium";
        char[] actualToCharArray = actual.toCharArray();
        String reverse="";

        // 1st way
        for(int i = actualToCharArray.length-1; i>=0; i--){
            reverse= reverse + actualToCharArray[i];
        }
        System.out.println(reverse.trim());

        // 2nd way
        for (int j = actual.length()-1; j>=0 ; j--){
            reverse = reverse+actual.charAt(j);
        }
        System.out.println(reverse.trim());

        //3rd way
        StringBuilder sb = new StringBuilder(actual);
        System.out.println(sb.reverse());
    }
}
