package practice;

import java.util.ArrayList;
import java.util.List;

public class RemoveSpecialCharacters {


    public static void logicTwo(){
        String wrongText = "Awesome %$#@@@ Aw$&2One2three";

        String wrongTextRemovingSplChars= wrongText.replaceAll("[%$#@&]"," ");

        System.out.println(wrongTextRemovingSplChars);
    }

    public static void main(String[] args) {

//        String wrongText = "Awesome %$#@@@ Aw$&2One2three";
//
//        String[] wrongTextToArray = wrongText.split(" ");
//
//        String correctText ="";
//
//
//        for(int i=0; i<= wrongTextToArray.length-1;i++){
//            if(wrongTextToArray[i].contains("%")||
//                    wrongTextToArray[i].contains("@")||
//                    wrongTextToArray[i].contains("$")||
//                    wrongTextToArray[i].contains("2")){
//                correctText = correctText + " " + wrongTextToArray[i].replaceAll("[%$#@2&%^$#!()\\,\\.]","");
//            } else {
//                correctText = correctText + " " + wrongTextToArray[i];
//            }
//        }
//        System.out.println(correctText);

        logicTwo();
    }
}
