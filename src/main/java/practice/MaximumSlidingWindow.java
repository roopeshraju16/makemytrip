package practice;

import java.util.Scanner;

public class MaximumSlidingWindow {

    public static void main(String[] args) {
        int a[]={ 1, 4, 2, 10, 2, 3, 1, 0, 20};
        int k =4;
        int N = a.length-1;
        int max_Sum=0;
        int win_Sum=0;
        for(int i=0;i< N-k;i++){
            win_Sum = 0;
            for(int j=i;j< i+k;j++){
                win_Sum+=a[j];
            }
            max_Sum = Math.max(win_Sum,max_Sum);
        }

        System.out.println(max_Sum);

        String[] arr = {"Even", "Odd"};

        Scanner s = new Scanner(System.in);

        System.out.print("Enter the number: ");
        int no = s.nextInt();

        System.out.println(arr[no%2]);

        printNos(100);


    }


    static void printNos(int n)
    {
        if(n > 0)
        {
            printNos(n - 1);
            System.out.print(n + " ");
        }
        return;
    }

}
