package practice;

import java.util.*;

public class Triplets {

    public static  int findTriplets(int[] b){
        int count=0;
        HashSet<Integer> set = new HashSet<>();
        for(int a=0;a<=b.length-1;a++){
            set.add(b[a]);
        }

        for(int i =0;i<=b.length-1; i++){
            for(int j=i; j<= b.length-1; j++){
                int sum=b[i]+b[j];
                if(set.contains(sum)){
                    count++;
                }
            }
        }

        return count;
    }


    public static int secondLogic(int[] c){
        int count =0;
        for(int i=0;i<=c.length-1; i++){
            for(int j=i+1; j<=c.length-1; j++){
                for(int k=i+2;k<=c.length-1; k++){
                    if(c[i]+c[j]==c[k]){
                        count++;
                    }

                }
            }
        }
        return count;
    }

    public static int[] values(int numberOfElements){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the length:");
        int n = sc.nextInt();
        int[] a = new int[n];
        System.out.println("Enter the array elements:");
        for(int i=0;i<=a.length-1;i++){
            a[i]=sc.nextInt();
        }
        return a;
    }


    public static void main(String[] args) {
        System.out.println("This is the number of tripples:" +secondLogic(values(3)));

    }

}
