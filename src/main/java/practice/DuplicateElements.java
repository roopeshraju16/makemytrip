package practice;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DuplicateElements {

    public static void findDuplicateElements(){
        String[] strArray = {"java", "javaScript", "python", "java", "c++", "c++", "c"};
        HashMap<String,Integer> map = new HashMap<>();

        for(int i=0; i<=strArray.length-1; i++){
            if(map.containsKey(strArray[i])){
                map.put(strArray[i],map.get(strArray[i])+1);
            }else {
                map.put(strArray[i],1);
            }
        }

        for(Map.Entry m:map.entrySet()){
            if((Integer)m.getValue() > 1){
                System.out.println("duplicate values:"+ m.getKey());
            }
        }
    }


    public static void findDuplicatesUsingHashSet(){
        String[] strArray = {"java", "javaScript", "python", "java", "c++", "c++", "c"};
        Set<String> set = new HashSet<>();
        for(int i=0; i<=strArray.length-1; i++){
             set.add(strArray[i]);
        }
        for(String s1: set){
            System.out.println(s1);
        }

    }


    public static  void findAndDeleteDuplicates(){
        String[] strArray = {"java", "javaScript", "python", "java", "c++", "c++", "c"};
        HashMap<String,Integer> map = new HashMap<>();

        for(int i=0; i<=strArray.length-1; i++){
            if(map.containsKey(strArray[i])){
                map.remove(strArray[i]);
            }else {
                map.put(strArray[i],1);
            }
        }
        System.out.println("Array without duplicates:"+map);
    }



    public static void main(String[] args) {
//        findDuplicateElements();
        findDuplicatesUsingHashSet();
    }
}
