package practice;

public class AddOfStringToInt {

    public static int numberOne(String num1){
        return Integer.parseInt(num1);
    }

    public static int numberTwo(String num2){
        return Integer.parseInt(num2);
    }

    public static void main(String[] args) {

        String num1 ="11";
        String num2 ="123";

        String conversionValue=String.valueOf(numberOne(num1)+numberTwo(num2));

        System.out.println("Converted Value:"+conversionValue);
    }

}