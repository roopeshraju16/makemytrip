package practice;

import java.util.TreeSet;

public class RearrangingArray {

    public static void logic_One(){
        int a[] = {1,2,3,4,5,6};
        int temp;
        for(int i=0;i<=a.length-1;i++){
            for(int j=i+1; j<=a.length-1; j++){
                if(a[i]<a[j]){
                    temp=a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
        }
        for(int k=0;k<=a.length-1;k++){
            System.out.println(a[k]);
        }
    }

    public static void logic_Two(){
        int a[] = {1,2,3,4,5,6};
        TreeSet<Integer> ts =new TreeSet<>();
        for(int i=0; i<=a.length-1;i++){
            ts.add(a[i]);
        }
        System.out.println(ts.descendingSet());
    }

    public static void main(String[] args) {
        logic_One();
        logic_Two();
    }
}
