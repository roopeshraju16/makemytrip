package practice;


public class AdditionOfNumbersInBtwString {

    public static int getNumber(){
        String s="Welcome123To456India789";
        String temp ="0";
        int sum=0;
        for(int i=0;i<=s.length()-1;i++){
            char ch = s.charAt(i);
            if(Character.isDigit(ch)){

                  temp+=ch;
            }
            else {
                sum+=Integer.parseInt(temp);
                temp="0";
            }
        }
         return sum+Integer.parseInt(temp);
    }

    public static void main(String[] args) {
        System.out.println(getNumber());
    }
}
