package practice;

public class PrimeNumber {

    //2 is the least prime number
    // 3 is the least

    //By default 0 and 1 are prime numbers

    public static boolean isPrime(int num){

        boolean flag = false;
        if(num<=1){
            return  flag;
        }

        for(int i=2; i<num; i++){
            if(num%i ==0){
                return flag;
            }
        }
        flag = true;
        return  flag;
    }


    public static void printPrime(int n){
        for(int i=3; i<=n; i++){
            if(isPrime(i)){
                System.out.println("print the prime number list:"+i);
            }
        }
    }

    public static void main(String[] args) {
        printPrime(9);
    }
}
