package practice;

import java.util.*;

public class Collections {

    public void listUsingIterator(){
        ArrayList<String> al = new ArrayList<>();
        al.add("Hi");
        al.add("wassup");
        al.add("do something");

        Iterator it = al.iterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }
    }

    public void listUsingForLoop(){
        ArrayList<String> al = new ArrayList<>();
        al.add("Hi");
        al.add("wassup");
        al.add("do something");
        al.add(1,"comedy");
        for(int i=0; i<= al.size()-1 ; i++){
            System.out.println(al.get(i));
        }
    }

    public void linkedListIterator(){
        LinkedList<String> ll = new LinkedList<>();
        ll.addFirst("Hey!");
        ll.add("how are you doing?");
        ll.addLast("Bye");
        Iterator Link= ll.iterator();
        while (Link.hasNext()){
            System.out.println(Link.next());
        }
    }


    public void hashSet(){
        HashSet<String> hs = new HashSet<>();
        hs.add("Hey");
        hs.add("Hey");
        Iterator i = hs.iterator();
        while (i.hasNext()){
            System.out.println(i.next());
        }
    }


    public void findOccurance(){
        String strOccurance="HJKJKJKLM";
        char[] strToCharArray = strOccurance.toCharArray();
        HashMap<Character,Integer> hMap = new HashMap<>();
        for(int i=0;i<=strToCharArray.length-1;i++){
            if(hMap.containsKey(strToCharArray[i])){
                hMap.put(strToCharArray[i],hMap.get(strToCharArray[i])+1);
            }else {
                hMap.put(strToCharArray[i],1);
            }
        }
        System.out.println(hMap);
    }

    public void findWordOcurance(){
        String paragraph = "Hey wassup what doing? what about your plan wassup with you";
        String[] strParaArray = paragraph.split(" ");
        HashMap<String,Integer> hMap = new HashMap<>();
        for(int i=0; i<=strParaArray.length-1;i++){
            if(hMap.containsKey(strParaArray[i])){
                hMap.put(strParaArray[i],hMap.get(strParaArray[i])+1);
            } else {
                hMap.put(strParaArray[i],1);
            }
        }
        System.out.println(hMap);
    }

    public void findingLongestPalindrome(){
        String longestParagraph = "AAA BBBB dccd ABABAB";
        String[] convertingToStringArray = longestParagraph.split(" ");

        for(int i=0; i<=convertingToStringArray.length-1 ;i++){
             String sNewValue = convertingToStringArray[i];
             char[] chars = sNewValue.toCharArray();
             String sNew = "";
             LinkedHashMap<String,Integer> linkedHashMap = new LinkedHashMap<>();
               for(int j =chars.length-1 ; j>=0 ; j--){
                     sNew = sNew+chars[j];
                     sNew.trim();
                    linkedHashMap.put(sNew,sNew.length());
               }
            if(sNew.equalsIgnoreCase(sNewValue)){
                System.out.println(linkedHashMap.get(sNew));
            }
        }
    }

    public static void main(String[] args) {
        Collections collections = new Collections();
        collections.findingLongestPalindrome();
    }
}
