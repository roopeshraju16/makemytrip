package practice;

public abstract class Hidden {

    abstract void setSalary(int salary);
    abstract int getSalary();
    void display(){
        System.out.println();
    }
}

class value extends Hidden {

    int salary;
    @Override
    void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    int getSalary() {
        return salary;
    }

    void display(){
        System.out.println(getSalary());
    }

    public static void main(String[] args) {
        value v1 = new value();
        v1.setSalary(10000);
        v1.display();
    }
}

