package bigbasket;

import io.restassured.RestAssured;

public class Api {


    public static void getRequest(){
        RestAssured.given().header("Content-Type","application/json")
                .queryParam("q","orange").log().all().when().get("https://www.bigbasket.com/ps/").then().assertThat().statusCode(200);
    }


    public static void main(String[] args) {
        getRequest();
    }
}
