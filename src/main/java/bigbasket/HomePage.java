package bigbasket;

import org.openqa.selenium.By;

public class HomePage extends  BasePage{




    public void launchHomePage(){
        launchBrowser();
        driver.get("https://www.bigbasket.com/");
    }

    public void clickSearchBar(){
        driver.findElement(By.cssSelector("[qa='searchBar']")).sendKeys("Orange");
        driver.findElement(By.cssSelector("[qa='searchBtn']")).click();
    }

}
