package page;

import utilities.TestBasePage;

import java.io.IOException;

public class HomePage extends TestBasePage {

    public void launchBrowser() {
        try {
            if(fetchProperties("browser").equalsIgnoreCase("chrome")){
                launchChrome();
            } else if(fetchProperties("browser").equalsIgnoreCase("firefox")){
                launchFirefox();
            }
        }catch (IOException io){
            io.printStackTrace();
        }
    }


    public void setupAmazon(){
        getDriver().get("www.amazon.in");
        getDriver().manage().window().maximize();
    }


    public void setupFlipkart(){
        getDriver().get("www.flipkart.com");
        getDriver().manage().window().maximize();
    }

    public void quitBrowse(){
        getDriver().quit();
    }


}
