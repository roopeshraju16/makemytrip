package thread;

import page.HomePage;

public class EcommerceThread extends  Thread{
    String  test;
    HomePage homePage = new HomePage();

    public EcommerceThread(String threadName,String test){
        super(threadName);
        this.test = test;
    }

    @Override
    public void run(){
        System.out.println("Thread started" +Thread.currentThread().getName());

        try{
            Thread.sleep(1000);
            homePage.setupAmazon();
            homePage.setupFlipkart();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            homePage.quitBrowse();
        }
        System.out.println("Thread ended" +Thread.currentThread().getName());
    }
}
