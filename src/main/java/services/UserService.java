package services;

import apiendpoints.ApiEndPoints;
import io.cucumber.messages.internal.com.fasterxml.jackson.core.JsonProcessingException;
import io.cucumber.messages.internal.com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import pojo.Users;
import pojo.UsersResponse;
import utilities.ApiService;

import java.io.File;

public class UserService  extends ApiService {

       Users users = new Users();
       UsersResponse usersResponse = new UsersResponse();

       public String URI = "https://reqres.in/";

       public Response createRequestForUsers(String name,String job){
           users.setName(name);
           users.setJob(job);
           Response response = RestAssured.given().header("Accept","application/json")
                   .header("Content-Type","application/json")
                   .log().all().when()
                   .body(users).post(URI+ ApiEndPoints.createUsers).then().assertThat().statusCode(200).extract().response();
           System.out.println(response.getBody().asString());
           return  response;

       }

       public void createUsersResponse(Response response) throws JsonProcessingException {
           JsonPath path = response.getBody().jsonPath();
           usersResponse.setId(path.getString("id"));
           usersResponse.setCreatedAt(path.getString("createdAt"));
           usersResponse.setName(path.getString("name"));
           usersResponse.setJob(path.getString("job"));

           ObjectMapper om = new ObjectMapper();
           String responsValue = om.writeValueAsString(usersResponse);
           System.out.println(responsValue);
       }


       public void createMultiPartRequest() {
            RestAssured.given().headers("content-Type", "application/json")
                   .headers("Accept", "application/json")
                   .queryParam("ssh", "123").log().all()
                   .when().multiPart(new File("")).body(users).then().statusCode(200);
       }

       public void response(Response response){
           JsonPath jP = response.getBody().jsonPath();
           usersResponse.setJob(jP.getString("job"));
       }

       //Handling dynamic
    public void dynamic(){
        
    }


    public static void main(String[] args) throws JsonProcessingException {
         UserService userService = new UserService();
         userService.createUsersResponse(userService.createRequestForUsers("Phani","Lead"));
    }
}
