package pojo;

public class Users {

    String name;
    String job;

    public void setName(String name){
        this.name = name;
    }

    public void setJob(String job){
        this.job = job;
    }

    public String getJob(){
        return job;
    }

    public String getName(){
        return name;
    }
}
