package pojo;

public class UsersResponse {
    String name;
    String job;
    String id;
    String createdAt;

    public void setName(String name){
        this.name = name;
    }

    public void setJob(String job){
        this.job = job;
    }

    public String getJob(){
        return job;
    }

    public String getName(){
        return name;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setCreatedAt(String createdAt){
        this.createdAt = createdAt;
    }
}
