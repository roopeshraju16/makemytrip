package flights;

public class Status {

    String result;
    Message message;

    public void setResult(String result){
        this.result = result;
    }

    public String getResult(){
        return result;
    }


    public void setMessage(Message message){
        this.message = message;
    }

    public Message getMessage(){
        return message;
    }
}
