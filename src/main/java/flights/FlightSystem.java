package flights;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class FlightSystem {


//    https://travel.paytm.com/api/flights/v2/search?adults=1&children=0&class=E&client=web&departureDate=20220218&origin=DEL&infants=0&destination=BLR



    public static Response fetchFlightResponse(int adults, int children, String classValue, String departureDate, String origin, int infants, String destination){
       Response response = RestAssured.given().headers("Accept","application/json")
                .queryParam("adults",adults).queryParam("children",children)
                .queryParam("class",classValue).queryParam("client","web")
                .queryParam("departureDate",departureDate).queryParam("origin",origin)
                .queryParam("destination",destination).queryParam("infants",infants).log().all().when()
                .get("https://travel.paytm.com/api/flights/v2/search")
                .then().assertThat().statusCode(200).extract().response();

       return response;
    }

    public static void printResponse(Response response){
        Status status = new Status();
//        System.out.println(response.prettyPrint());
        status.setResult(response.jsonPath().get("result"));
        status.setMessage(response.jsonPath().get("status.message"));
        System.out.println(status.getResult()+""+status.getMessage());
    }

    public static void main(String[] args) {
        printResponse(fetchFlightResponse(1,0,"E","20220218","DEL",0,"BLR"));
    }

}
