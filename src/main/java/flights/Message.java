package flights;

public class Message {

    String title;
    String message;

    public void setTitle(String title){
        this.title= title;
    }

    public String getTitle(){
        return title;
    }
    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }
}
