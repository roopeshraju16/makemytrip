package utilities;

import java.util.HashMap;
import java.util.Map;

public class FindMaxStringNumber {


    public static void fetchMaxRepeatingCharacter(){
        String s = "aaabbcbb";
        Map<Character,Integer> mp = new HashMap<>();
        for(int i=0;i<=s.length()-1;i++){
            char c = s.charAt(i);
            if(mp.containsKey(c)){
                mp.put(c,mp.get(c)+1);
            }else {
                mp.put(c,1);
            }
        }


        int[] a = new int[2];
        int i=0;
        while (i<=a.length-1){
        for(Map.Entry<Character,Integer> map:mp.entrySet()){
                if (map.getValue()>=2){
                    a[i]=map.getValue();
                    i++;
                }
            }
        }

        int maximumValue = Math.max(a[0],a[0+1]);

        for(Map.Entry<Character,Integer> map:mp.entrySet()){
                if(maximumValue==map.getValue()){
                    System.out.println(map.getValue()+" is the maximum "+map.getKey());
                }
        }


    }

    public static void main(String[] args) {
        fetchMaxRepeatingCharacter();
    }
}
