package problemsolving;

public class Logic {


    public static int getJumps(int[] a){
        int jumpCount =0;
        int i=0;

            while (i<=a.length-1){
                i=i+a[i];
                jumpCount++;
                if(String.valueOf(i).contains("-")){
                    return -1;
                }
            }
        return jumpCount;
    }

    public static void main(String[] args) {
        int[] a = {1,2,0,-2};
        System.out.println(getJumps(a));
    }
}
