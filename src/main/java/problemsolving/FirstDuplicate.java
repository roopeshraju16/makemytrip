package problemsolving;

import java.util.HashMap;

public class FirstDuplicate {

//    aweqserswa

    public static void fetchDuplicates(){
        String s = "aweqserswa";
        HashMap<Character,Integer> map = new HashMap<>();
        int count=0;
        for(int i=0;i<=s.length()-1;i++) {
            if (map.containsKey(s.charAt(i))) {
                count++;
                map.put(s.charAt(i), map.get(s.charAt(i)) + 1);
                System.out.println(count + " Duplicate:" + s.charAt(i) + ": " + count);
            } else {
                map.put(s.charAt(i), 1);
            }
        }
    }


    public static void main(String[] args) {
        fetchDuplicates();
    }

}
