package problemsolving.Ms;

import java.util.HashMap;
import java.util.Map;

public class RemoveAdjacentDuplicatesByKValue {

    public String removeDuplicates(String s, int k) {
        char[] sToCharArray = s.toCharArray();
        HashMap<Character,Integer> map = new HashMap<>();
        String res="";

        for(int i=0; i<=sToCharArray.length-1; i++){

            if(map.containsKey(sToCharArray[i])){
                map.put(sToCharArray[i],map.get(sToCharArray[i])+1);
            }else{
                map.put(sToCharArray[i],1);
            }

        }

        for(Map.Entry<Character,Integer> mapOne : map.entrySet()){
            if(!(mapOne.getValue() >=k)){
                res+=mapOne.getKey();
            }
        }

        return res;
    }

}
