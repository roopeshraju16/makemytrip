package problemsolving.Ms;

import java.util.TreeSet;

public class SortEvenOdd {

    public static int[] fetchEvenOddArray(){
          int[] a = {1, 2, 3, 5, 4, 7, 10};
        TreeSet<Integer> evenSet = new TreeSet<>();
        TreeSet<Integer> oddSet  = new TreeSet<>();
          for(int i=0;i<=a.length-1; i++){
              if(a[i]%2==0){
                  evenSet.add(a[i]);
              }
              else {
                  oddSet.add(a[i]);
              }
          }
          int[] b = new int[evenSet.size()+oddSet.size()];
          int count=0;

          for(int j: oddSet.descendingSet()){
              b[count] = j;
              count++;
          }

          for(int k: evenSet){
              b[count] = k;
              count++;
          }
          return b;
    }


    public static void main(String[] args) {
        for(int v=0; v<=fetchEvenOddArray().length-1; v++){
            System.out.println(fetchEvenOddArray()[v]);
        }
    }
}
