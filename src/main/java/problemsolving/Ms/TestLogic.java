package problemsolving.Ms;

import java.util.*;

public class TestLogic {


    public static String getDecodeValue(String s){
        String decodedValue ="";
        char[] convertionOfStoCharArray = s.toCharArray();
        Stack<Integer> integerStack = new Stack<>();
        Stack<String> stringStack = new Stack<>();
        int ptr=0;

        if(s.length()==0){
            return  null;
        }
        while (ptr<=convertionOfStoCharArray.length-1){
            if(Character.isDigit(convertionOfStoCharArray[ptr])){
                int num =0;
                while (Character.isDigit(convertionOfStoCharArray[ptr])){
                   num = num*10+convertionOfStoCharArray[ptr]-'0';
                   ptr++;
                }
                integerStack.push(num);
            }else if(convertionOfStoCharArray[ptr]=='['){
                stringStack.push(decodedValue);
                decodedValue="";
                ptr++;
            }else if(convertionOfStoCharArray[ptr]==']'){
                StringBuilder sb = new StringBuilder(stringStack.pop());
                int count = integerStack.pop();
                for(int i=0;i<=count;i++)
                    sb.append(decodedValue);

                decodedValue = sb.toString();
                ptr++;
            }else {
                decodedValue=decodedValue+convertionOfStoCharArray[ptr];
                ptr++;
            }
        }

        return decodedValue;
    }

    public static String removeAdjacentDuplicates(String s){
        char[] cToS =  s.toCharArray();
        int ptr=0;
        Stack<Character> sC = new Stack<>();
        String removedDuplicates ="";
        while (ptr<=cToS.length-1){
            if((!sC.isEmpty())&&(sC.peek()==cToS[ptr])){
                sC.pop();
            }else {
                sC.push(cToS[ptr]);
            }
            ptr++;
        }

        removedDuplicates = removedDuplicates+sC;

        return removedDuplicates;
    }

    public static int[] arrangeOddAndEvenNumbers(){
        int[] evenOdd = new int[5];
        return evenOdd;
    }

    public static int fetchStockValue(int[] a){
        int profitValue=0;
        int ptr=1;
        while (ptr<=a.length-1){
            if(a[ptr] > a[ptr-1]){
                profitValue = profitValue+ a[ptr] - a[ptr-1];
            }
            ptr++;
        }
        return profitValue;
    }

    public static int boatProb(){
        int[] a ={3,2,2,1};
        int i=0;
        int k=4;
        int count=0;
        while (i<=a.length-1){
            if(a[i]+a[i+1]<=k){
                count++;
            }
            if(i+1==a.length-1)
                break;

            i++;
        }

        return count;
    }

    public static List<List<String>>  fetchAnagrams(String[] strs){
        HashMap<String, ArrayList<String>> map = new HashMap<>();
        ArrayList<List<String>> ls = new ArrayList<>();
         for(int i=0; i<=strs.length-1;i++){
             char[] strsToC = strs[i].toCharArray();
             Arrays.sort(strsToC);
             String sorted = new String(strsToC);

             ArrayList<String> s = map.getOrDefault(sorted,new ArrayList<>());

             s.add(String.valueOf(strsToC));

             map.put(sorted,s);
         }

         for(Map.Entry<String,ArrayList<String>> m:map.entrySet()){
             ArrayList<String> L = m.getValue();
             ls.add(L);
         }

         return ls;
    }

    public static void main(String[] args) {
        System.out.println(getDecodeValue("3[b2[ca]]"));
        System.out.println(removeAdjacentDuplicates("geeksForgeeg"));
        int[] stockValues = {100, 180, 260, 310,
                40, 535, 695};
        System.out.println(fetchStockValue(stockValues));
        System.out.println(boatProb());
        String[] s = {"eat","tea","tan","ate","nat","bat"};
        System.out.println(fetchAnagrams(s));
    }
}
