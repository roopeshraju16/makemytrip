package problemsolving.Ms;

public class Rotate {

    public static void rotateArray(){

        int numberOfRotations= 3;
        int[] a={1,2,3,4,5};//0,1,2,3,4
        int[] newArray = new int[a.length];
        int splitPosition = numberOfRotations;
        int count=0;
        while (splitPosition<=a.length-1){
            newArray[count]=a[splitPosition];
            count++;
            splitPosition++;
        }

        int i=0;
        while (i<numberOfRotations){
            newArray[count]=a[i];
            count++;
            i++;
        }

        for(int j=0;j<=newArray.length-1;j++){
            System.out.println(newArray[j]);
        }
    }

    public static void permuteString(String str,String ans){
        if(str.length()==0){
            System.out.println(ans+" ");
            return;
        }


        for(int i=0;i<=str.length()-1;i++){
           char c = str.charAt(i);

            String value= str.substring(0,i)+str.substring(i+1);
            permuteString(value,ans+c);
        }
    }

    public static void reverseOfStringWithOutRechangingSplCharacters(){
        String s ="ab!@#roopesh$%";

        char[]c = s.toCharArray();
        int i =0;
        int last = c.length-1;
        char temp;
        while (i<last){

            if(!Character.isAlphabetic(c[i])){
                i++;
            }

            if(!Character.isAlphabetic(c[last])){
                last--;
            }

            if(Character.isAlphabetic(c[i])&&Character.isAlphabetic(c[last])){
                temp = c[i];
                c[i] = c[last];
                c[last] = c[i];

                i++;
                last--;
            }
        }
        System.out.println(new String(c));

    }

    public static void main(String[] args) {
//        rotateArray();
//        permuteString("ABC","");
        reverseOfStringWithOutRechangingSplCharacters();
    }
}
