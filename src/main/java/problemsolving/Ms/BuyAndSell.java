package problemsolving.Ms;

public class BuyAndSell {

    public static int value(int[] a){
        int profitValue =0;
        int i=1;
        while (i<=a.length-1){
          if(a[i]>a[i-1]){
              profitValue += a[i] - a[i-1];
          }
          i++;
        }
        return profitValue;
    }

    public static void main(String[] args) {
        int[] stockValues = {100, 180, 260, 310,
                40, 535, 695};
        System.out.println(value(stockValues));
    }
}
