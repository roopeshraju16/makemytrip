package problemsolving.Ms;

import java.util.Stack;

public class AdjacentDuplicates {

    public static String fetchByRemovingAdjacentDuplicates(String strs){

        char[] stringToChars = strs.toCharArray();
        Stack<Character> characterStack = new Stack<>();
        int i=0;
        String res="";
        while (i<=stringToChars.length-1){
          if((!characterStack.isEmpty()) && (characterStack.peek()==stringToChars[i])){
              characterStack.pop();
           }else {
              characterStack.push(stringToChars[i]);
          }
            i++;
        }
        res+=characterStack;
        return res;
    }



    public static void main(String[] args) {
        System.out.println(fetchByRemovingAdjacentDuplicates("geeksforgeeg"));
    }
}
