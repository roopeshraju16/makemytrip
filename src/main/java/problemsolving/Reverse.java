package problemsolving;

public class Reverse {

    public static String reverse(String str){

     String[] splitToStringArray = str.split(" ");
     String reversed= "";

     for(int i =splitToStringArray.length-1; i>=0; i--){
           reversed +=splitToStringArray[i] +" ";
     }
       return  reversed.trim();
    }

    public static String reverseCharacter(String str){
        char[] c = str.toCharArray();
        String reverseCharacter ="";
        for(int i=c.length-1; i>=0; i--){
            reverseCharacter+= c[i];
        }
        return reverseCharacter;
    }

    public static void swap(int a,int b){
        System.out.println("before swap:"+a +" "+b);
        a = a+b; // 22
        b = a-b; //10
        a = a-b; //12
        System.out.println("after swap: a ="+a +"  b="+b);

    }

    public static void reverStringWithoutTouchingSplCharacters(String str){
      char[] ch =  str.toCharArray();
      int first=0; int last = ch.length-1;
      char temp;

      while (first<last){
          if(!Character.isAlphabetic(ch[first])){
              first++;
          }
          if(!Character.isAlphabetic(ch[last])){
              last--;
          }
          if((Character.isAlphabetic(ch[first])) && (Character.isAlphabetic(ch[last]))){
              temp = ch[first];
              ch[first] = ch[last];
              ch[last] = temp;
              first++;
              last--;
          }
      }
        System.out.println(new String(ch));
    }


    public static void main(String[] args) {
//        String s = "Blackhawk"; //Network Blackhawk
//        System.out.println(reverseCharacter(s));
//        int a =10;
//        int b = 12;
//        swap(a,b);
        reverStringWithoutTouchingSplCharacters("a123hgfj4kl567e8gh");
    }
}
