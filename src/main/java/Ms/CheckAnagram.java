package Ms;

import java.util.Arrays;

public class CheckAnagram {


    public static boolean checkAnagaram(String s1,String s2){
        char[] c1= s1.toCharArray();
        char[] c2= s2.toCharArray();
        Arrays.sort(c1);
        Arrays.sort(c2);


        if(c1.length!=c2.length)
            return false;

        for(int i=0;i<=c1.length-1;i++)
            if(c1[i]!=c2[i])
                return false;
            return true;
    }

    public static void main(String[] args) {
        System.out.println(checkAnagaram("traveloka","traveling"));
//        traveloka,traveling
    }
}
