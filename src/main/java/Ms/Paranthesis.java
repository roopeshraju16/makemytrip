package Ms;

import java.util.Stack;

public class Paranthesis {

    public static boolean value(String str){
        boolean flag = false;
        char[] c = str.toCharArray();
        Stack<Character> stack = new Stack<>();
        if(str == null){
            return flag;
        }
        else {
            for(int i=0; i<=c.length-1; i++) {

                if(c[i]=='['||c[i]=='{'||c[i]=='(') {
                    stack.push(c[i]);

                }
                char value;
                switch (c[i]){
                    case ']':
                        value=stack.pop();
                        if(value=='{'||value=='(')
                             return flag;
                        break;
                    case '}':
                        value=stack.pop();
                        if(value=='['||value=='(')
                             return flag;
                        break;
                    case ')':
                        value=stack.pop();
                        if(value=='['||value=='{')
                           return flag;
                        break;
                }

            }
        }
        return (stack.isEmpty());
    }

    public static void main(String[] args) {
        if(value("[(])")){
            System.out.println("Balanced");
        }else {
            System.out.println("UnBalanced");
        }
    }
}
