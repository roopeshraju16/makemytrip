package Ms;

import java.util.ArrayList;
import java.util.List;

public class People {

    //List of people weight =3,2,2,1
    //Target Limit based upon the weight = 4
    // weight of the target can't be more than the limit
    //Combination of people

    public static void logic(List<Integer> lengthOfList,int target){
        int count =0;
        for(int i=0; i<=lengthOfList.size()-1;i++){
            int k =lengthOfList.get(i)+lengthOfList.get(i+1);
            if (k<=target){
                count++;
            }
            if(i+1==lengthOfList.size()-1)
              break;
        }
        System.out.println(count);
    }

    public static List<Integer> inputValues(){
        List<Integer> lW = new ArrayList<>();
        lW.add(3);
        lW.add(2);
        lW.add(2);
        lW.add(1);
        return lW;
    }

    public static void main(String[] args) {
        logic(inputValues(),4);
    }

}
