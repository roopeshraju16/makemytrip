package Ms;

import java.util.Stack;

public class Decoding {

    public static String decode(String str){
       char[] c =  str.toCharArray();
        Stack<String> stackChar = new Stack<>();
        Stack<Integer> stackInt = new Stack<>();
        int ptr =0;
        String res="";
        while (ptr<=c.length-1){
            if(Character.isDigit(c[ptr])){
                int num =0;
                while (Character.isDigit(c[ptr])){
                    //32
                   num = num*10+c[ptr]-'0';
                   ptr++;
                }
                stackInt.push(num);
            }
            else if(c[ptr]=='['){
                stackChar.push(res);
                res="";
                ptr++;
            }
            else if(c[ptr]==']'){
                StringBuilder sb = new StringBuilder(stackChar.pop());
                int count = stackInt.pop();
                for(int i=1;i<=count;i++)
                       sb.append(res);

                res =sb.toString();
                ptr++;
            }
            else {
                res = res + c[ptr];
                ptr++;
            }
        }
      return res;
    }

    public static void main(String[] args) {
        System.out.println(decode("3[b2[ca]]"));
    }
}
