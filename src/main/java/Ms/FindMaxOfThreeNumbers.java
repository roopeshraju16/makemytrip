package Ms;

public class FindMaxOfThreeNumbers {

    public static void main(String[] args) {
        int[] a ={1,2,3,4};

        int maxProduct = Integer.MIN_VALUE;
        for(int i=0;i<=a.length-1;i++){
            for(int j=i+1; j<=a.length-1; j++){
                for(int k=j+1; k<=a.length-1; k++){
                    maxProduct =  Math.max(maxProduct,a[i]*a[j]*a[k]);
                }
            }
        }
        System.out.println(maxProduct);
    }
}
