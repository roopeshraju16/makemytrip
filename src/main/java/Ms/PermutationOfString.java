package Ms;

public class PermutationOfString {

    public static void permute(String  str,String ans){
        if(str.length()==0){
            System.out.println(ans+" ");
            return;
        }


        for(int i=0;i<str.length()-1;i++){
            char c = str.charAt(i);
            String value = str.substring(0,i).substring(i+1);
            permute(value,ans+c);
        }
    }

    public static void main(String[] args) {

    }


}
