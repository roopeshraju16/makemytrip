package AwesomeTwo;

import java.util.HashMap;
import java.util.Map;

public class Occurance {

    String s = "anagram";
    char[] ch = s.toCharArray();


    public void logic() {
        HashMap<Character, Integer> map = new HashMap<Character, Integer>();
        for (int i = 0; i <= s.length() - 1; i++) {
            if(map.containsKey(ch[i])){
                map.put(ch[i],map.get(ch[i])+1);
            } else {
                map.put(ch[i],1);
            }
        }

        for (Map.Entry kv : map.entrySet()) {
            System.out.println(kv.getKey() + " " + kv.getValue());
        }
    }

    public static void main(String[] args) {
         Occurance occurance = new Occurance();
         occurance.logic();
    }
}

