import cart.Swiggy;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class UpdateCartSteps {


    Swiggy swiggy = new Swiggy();

    @Given("Setup the following request (.*) ,(.*), (.*), (.*),(.*),(.*)")
    public void setupTheFollowingRequestMenu_item_idQuantityRestaurantIdCart_typeAddress_idCouponCode(String menu_item_id
    ,long quantity,long restaurantId,String cart_type,String address_id,String couponCode) {
       swiggy.extractResponse(swiggy.setUpRequest(menu_item_id,quantity,restaurantId,cart_type,address_id,couponCode));
    }

    @Then("Print the following response")
    public void printTheFollowingResponse() {

    }
}
