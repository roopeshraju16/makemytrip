package tests;

import cucumber.api.java.en.Given;
import page.LoginPage;


public class LoginPageSteps {

    LoginPage loginPage = new LoginPage();


    @Given("^Login to make my trip$")
    public void loginToMakeMyTrip()  {
      loginPage.launchBrowser();
      loginPage.openUrl();
    }


}
