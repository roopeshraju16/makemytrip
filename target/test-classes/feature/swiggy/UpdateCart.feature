Feature: Hit the following Update cart API with different following set of data:

  Scenario Outline: Check the update cart API with different set of data
    Given Setup the following request <menu_item_id> ,<quantity>, <restaurantId>, <cart_type>,<address_id>,<couponCode>
    Then Print the following response
    Examples:
      | menu_item_id | quantity | restaurantId | cart_type | address_id | couponCode |
      |12345         | 1        |123           |REGULAR    |12356       | JUMBO      |
      |12345678      |10        |1245          |REGULAR    |145678      | SWIGGY     |
      |65438         |11        |1345          |REGULAR    |16789       | HDFC       |